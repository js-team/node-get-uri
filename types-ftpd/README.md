# Installation
> `npm install --save @types/ftpd`

# Summary
This package contains type definitions for ftpd (https://github.com/sstur/nodeftpd).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ftpd.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 12:01:52 GMT
 * Dependencies: [@types/node](https://npmjs.com/package/@types/node)
 * Global values: none

# Credits
These definitions were written by [Rogier Schouten](https://github.com/rogierschouten).
